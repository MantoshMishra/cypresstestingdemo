context('KrimzenDemo POC-Testing', () => {
  
  it('Login', () => {
    cy.visit(Cypress.env("url"))
    cy.get('#email').type(Cypress.env("email"))
    cy.get('#password').type(Cypress.env("password"))
    cy.contains('Login').click()
    cy.title().should('include', 'Krimzen')
  
  })

  it( 'Create_Category', ()=> {
    cy.contains('Add new').click()
    cy.contains('Category').click()
    cy.get('#compliance_category_id').select(Cypress.env("compliance_category_id"))
    cy.get('#name').type(Cypress.env("category_name"))
    cy.get('#description').type(Cypress.env("category_description"))
    cy.contains('Save').click()
    cy.title().should('include', 'Krimzen')
     })
  })